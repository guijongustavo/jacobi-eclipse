/*
 # Made by:
 # Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
 # Sergio Mendoza <sergio@astro.unam.mx>
 # Instituto de Astronomia UNAM
 # Ciudad Universitaria
 # Ciudad de Mexico
 # Mexico
 # Summer 2021
 #
*/

/* Headers*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_sf_elljac.h>
#include <gsl/gsl_sf_ellint.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft_complex.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

/* Thanks to Jim Weiss (https://stackoverflow.com/users/21005214/jim-weiss) to help in Stackoverflow: https://stackoverflow.com/questions/73468165/how-to-implement-progress-bars-in-c-like-apt-get for the macro of progress bar */

#define START_PROGRESS_BAR(pbar, len) \
    char __##pbar[len] = {0};         \
    memset(__##pbar, '#', sizeof(__##pbar) - 1)

#define PROGRESS_BAR_RUNING(pbar, per, fmt, ...) ({                                       \
    float p = per >= 100.0 ? 100.0 : per;                                                 \
    int left = (p / 100.0) * (sizeof(__##pbar) - 1);                                      \
    int right = (sizeof(__##pbar) - 1) - left;                                            \
    printf("\r[%.*s%*s] %.0f%%\n" fmt, left, __##pbar, right, "", (float)per, __VA_ARGS__); \
})

/* This opens the functions to be read and written */
void openfiles() ;

/* This closes the function to be written out */
void closefiles();

/* Datafile to be open */
FILE *datafilewrite;

/* Heaviside step function */
double step(double x);

void params(void);

double jacobi(double A, double m, double E, double S, double R, double V, double W, double std, double alpha);

/*Author of functions cnoise_generate_colored_noise and cnoise_uniform01 : Miroslav Stoyanov, Oak Ridge National Laboratory (https://people.sc.fsu.edu/~jburkardt/c_src/cnoise/cnoise.html) .*/	

double cnoise_uniform01 ( );

void cnoise_generate_colored_noise ( double *x, int N, double alpha, double std );

int main()
{	
	openfiles();
	params();
    	closefiles();
        START_PROGRESS_BAR(mybar, 64);
 	for (int i = 16; i < 32; i++)
     	{
         	PROGRESS_BAR_RUNING(mybar, (float)i / 31 * 100.0, " %d KB/s\n", 105);
 	        usleep(100000);
     	}
    return 0;
}

/*Enter inputs*/

 void params()
{

	double A;//Amplitude
	double m; // module or excentricity
	double E;//Eclipse
	double S;//Silence
	double R;// Resolution
	double V;//Vertical translate
	double W; //Waves
	double std; //Standar Desviation
	double alpha; //color

	printf("Enter the amplitud of eclipse:\n");
	scanf("%lf", &A);
	
	printf("Enter the period of eclipse:\n");
	scanf("%lf", &E);
	
	printf("Enter the period not eclipsed:\n");
	scanf("%lf", &S);
	
	printf("Enter the  vertical translation:\n");
	scanf("%lf", &V);
	
	printf("Enter the number of eclipses:\n");
	scanf("%lf", &W);
	
	printf("Enter the resolution r (0<r<1):\n");
	scanf("%lf", &R);
	
	printf("Enter the modulo m (0<m<1):\n");
	scanf("%lf", &m);
	
	printf("Enter the standard deviation:\n");
	scanf("%lf", &std);
	
	printf("Enter the color of noise: White (0), Pink (1) Brownian (2):\n");
	scanf("%lf", &alpha);
	
	jacobi(A,m,E,S,R,V,W,std,alpha);

}

	/* Jacobi */
	double jacobi(double A, double m, double E, double S, double R, double V, double W, double std, double alpha){
         START_PROGRESS_BAR(mybar, 64);
 for (int i = 0; i < 16; i++)
     {
         PROGRESS_BAR_RUNING(mybar, (float)i / 31 * 100.0, " %d KB/s\n", 105);
         usleep(100000);
     }


	/*variables and inputs*/
        double K, square_wave;
	double sn, cn, dn, u;
	double k = sqrt(m); //note the sqrt: m=k^2
	double PHI = M_PI/2;
	double c_y, c_x; //Coordenates
	int i = 0;
	/*This routine compute the incomplete elliptic integral F(phi,k)*/
	K = gsl_sf_ellint_F(PHI, k, 2);	

	//int N = (int)(W*((2.0*K+2.0*K*S/E)/R);
	int N = 10000;
	double count_time = 0;
        clock_t start, end;

	double *x = malloc( N * sizeof( double ) );
	for(int n=0;n<W;n++)
	{
		for(u=-2.0*K*S/E;u<=2.0*K;u+=R)
    		{
			start = clock();
		
			/*This function computes the Jacobian elliptic functions sn(u|m), cn(u|m), dn(u|m) by descending Landen transformations. */
			gsl_sf_elljac_e(u, m, &sn, &cn, &dn);
			cnoise_generate_colored_noise ( x, N, alpha, std );
			/*Square wave*/
			square_wave = step(u) - step(u-2*K);
			/*Coordenates*/
			c_x = E*u/(2*K) + (E + S)*n;
			c_y = square_wave*sn*A + V + x[i++];
			/*File*/
			fprintf(datafilewrite,"%f\t%f\n", c_x, c_y);

			end = clock();
			count_time += (end-start)/(double)CLOCKS_PER_SEC;
			printf( "Wait please... running %f seconds\n", count_time);
		}
	}
	free ( x );
	return 0;
}

double cnoise_uniform01 ( )

/******************************************************************************/
{
	return ( ( (double) rand() + 1.0 ) / ( (double) RAND_MAX + 1.0 ) );
};
/******************************************************************************/

void cnoise_generate_colored_noise ( double *x, int N, double alpha, double std )

/******************************************************************************/
{
	gsl_fft_complex_wavetable * wavetable;
	gsl_fft_complex_workspace * workspace;
	
	int i;
	double tmp;
	double gauss_u, gauss_v;
	double * fh = malloc( 4*N*sizeof(double) );
	double * fw = malloc( 4*N*sizeof(double) );
	
	fh[0] = 1.0; fh[1] = 0.0;
	for( i=1; i<N; i++ ){
		fh[2*i] = fh[2*(i-1)] * ( 0.5 * alpha + (double)(i-1) ) / ((double) i );
		fh[2*i+1] = 0.0;
	};
	for( i=0; i<N; i+=2 ){
		gauss_u = cnoise_uniform01(); gauss_v = cnoise_uniform01();
		fw[2*i] = std * sqrt( - 2 * log( gauss_u ) ) * cos( 2 * M_PI * gauss_v ); fw[2*i+1] = 0.0;
		fw[2*i+2] = std * sqrt( - 2 * log( gauss_u ) ) * sin( 2 * M_PI * gauss_v ); fw[2*i+3] = 0.0;
	};
	for( i=2*N; i<4*N; i++ ){ fh[i] = 0.0; fw[i] = 0.0; };
	
	wavetable = gsl_fft_complex_wavetable_alloc(2*N);
	workspace = gsl_fft_complex_workspace_alloc(2*N);

	gsl_fft_complex_forward (fh, 1, 2*N, wavetable, workspace);
	gsl_fft_complex_forward (fw, 1, 2*N, wavetable, workspace);
	
	for( i=0; i<N+1; i++ ){
		tmp = fw[2*i];
		fw[2*i]   = tmp*fh[2*i] - fw[2*i+1]*fh[2*i+1];
		fw[2*i+1] = tmp*fh[2*i+1] + fw[2*i+1]*fh[2*i];
	};

	fw[0] /= 2.0; fw[1] /= 2.0;
	fw[2*N] /= 2.0; fw[2*N+1] /= 2.0;
	
	for( i=N+1; i<2*N; i++ ){
		fw[2*i] = 0.0; fw[2*i+1] = 0.0;
	};
	
	gsl_fft_complex_inverse( fw, 1, 2*N, wavetable, workspace);
	
	for( i=0; i<N; i++ ){
		x[i] = 2.0*fw[2*i];
	};

	free( fh );
	free( fw );

	gsl_fft_complex_wavetable_free (wavetable);
	gsl_fft_complex_workspace_free (workspace);
};
/******************************************************************************/

double step(double x)
{
	if(x>=0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void openfiles()
{

//Open datafilewrite:
datafilewrite = fopen("output.dat", "w");

if (datafilewrite == NULL )
	{
	(void)printf("The data file output.dat was not OPEN \n\r"); 
	}
}

void closefiles()
{
  fclose(datafilewrite);
}
