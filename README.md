# Aztekas-Eclipse Software

Aztekas-eclipse is a module of Aztecas code, that produces artificial light with the use of Jacobi elliptical function programmed in C language. This use gsl librery (Gough 2009) and CNOISE code (Gough CNO).

With this, is posible modeling Amplitude, module or excentricity, Eclipse, Silence, Resolution, Vertical translate, Waves, Standar Desviation, color noise.

## How to install and run an example simulation

1. Open terminal in your computer.
2. Clone github repository: `$ git clone https://gitlab.com/guijongustavo/jacobi-eclipse.git`
3. Export the environment variable AZTEKAS_PATH `$ echo 'export AZTEKAS_PATH=$HOME/aztekas-main' >> ~/.bashrc` for Linux. 
4. Run `$ source ~/.bashrc`or `$ source ~/.bash_profile` in order to update the environment variable.
5. Compile: `$ gcc aztekas-eclipse.c -Wall -Wextra -lgsl -lm -o aztekas-eclipse`
6. Run the command `$ chmod 755 aztekas-eclipse`.
7. Run _aztekas-eclipse_: `./aztekas-eclipse`


## Usage

For run execute: 

./jacobi-gnuplot

Register yours values:

Enter the amplitud of eclipse
	
Enter the period of eclipse
	
Enter the period not eclipsed
	
Enter the  vertical translation
	
Enter the number of eclipses
	
Enter the resolution
	
Enter the modulo (0<m<1)
	
Enter the standard deviation
	
Enter the color of noise: White (0), Pink (1) Brownian (2)

# Authors

Gustavo Magallanes-Guijón <gustavo.magallanes.guijon@ciencias.unam.mx>
Sergio Mendoza <sergio@astro.unam.mx>
Instituto de Astronomia UNAM
Ciudad Universitaria
Ciudad de Mexico, Mexico





